$.question = '\
<li class="list-group-item">\
  <div class="input-group mb-3">\
    <div class="input-group-prepend">\
      <span class="input-group-text"><i class="fas fa-question-circle"></i></span>\
    </div>\
    <input type="text" class="form-control" name="question" placeholder="질문을 입력해주세요">\
    <div class="input-group-append">\
      <span class="input-group-text"><i class="fas fa-minus-circle" name="removeQuestion" data-toggle="tooltip" title="질문과 답변을 삭제합니다."></i></span>\
    </div>\
  </div>\
  <div name="answers">\
    <div class="input-group">\
      <div class="input-group-prepend">\
        <div class="input-group-text">\
          <input type="radio" disabled>\
        </div>\
      </div>\
      <input type="text" class="form-control" name="answer" placeholder="답변 입력해주세요">\
      <div class="input-group-append">\
        <span class="input-group-text"><i class="fas fa-minus" name="removeAnswer" data-toggle="tooltip" title="답변을 삭제합니다."></i></span>\
      </div>\
    </div>\
  </div>\
  <div class="text-center">\
    <i class="fas fa-plus" name="appendAnswer" data-toggle="tooltip" title="답변을 추가합니다."></i>\
  </div>\
</li>\
';

$.answer = '\
<div class="input-group">\
  <div class="input-group-prepend">\
    <div class="input-group-text">\
      <input type="radio" disabled>\
    </div>\
  </div>\
  <input type="text" class="form-control" name="answer" placeholder="답변을 입력해주세요">\
  <div class="input-group-append">\
    <span class="input-group-text"><i class="fas fa-minus" name="removeAnswer" data-toggle="tooltip" title="답변을 삭제합니다."></i></span>\
  </div>\
</div>\
';

$(function() {
  $('body').tooltip({
    selector: '[data-toggle=tooltip]'
  });

  $('[name=createSurvey]').on('click', function(e) {
    var survey = [];
    var items = $('.card').find('.list-group-item');

    items.each(function(index, item) {
      var question = $(item).find('[name=question]').val();
      var answers = [];
      $(item).find('[name=answer]').each(function(i, answer) {
        answers.push($(answer).val());
      });
      survey.push({question: question, answers: answers});
    });
    console.log(survey);
  });

  $('[name=appendQuestion]').on('click', function(e) {
    e.preventDefault();
    $('.card').append($.question);
  });

  $('body').on('click', '[name=removeQuestion]', function(e) {
    e.preventDefault();
    $(this).parents('li').remove();
  });

  $('body').on('click', '[name=appendAnswer]', function(e) {
    e.preventDefault();
    $(this).parents('.list-group-item').find('[name=answers]').append($.answer);
  });

  $('body').on('click', '[name=removeAnswer]', function(e) {
    e.preventDefault();
    $(this).parents('.input-group').remove();
  });
});
